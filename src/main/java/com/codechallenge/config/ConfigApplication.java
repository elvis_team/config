package com.codechallenge.config;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.cloud.config.server.EnableConfigServer;

/**
 * This class defines the central repository for all our microservices properties
 * This properties repository can be found in github (https://elvisols@bitbucket.org/elvis_team/configurations-repo)
 *
 * @author  Elvis
 * @version 1.0, 10/05/18
 */
@EnableConfigServer
@SpringBootApplication
@EnableDiscoveryClient
public class ConfigApplication {

	public static void main(String[] args) {
		SpringApplication.run(ConfigApplication.class, args);
	}
}
